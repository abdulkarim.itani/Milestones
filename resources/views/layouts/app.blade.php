<!DOCTYPE html>
<html>
    <head>
        <title>{{title}}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{% static 'homepage/css/uikit.min.css' %}" />
        <link rel="stylesheet" type="text/css" href="{% static 'homepage/css/custom.css' %}" />
        <script src="{% static 'homepage/js/uikit.min.js' %}"></script>
        <script src="{% static 'homepage/js/uikit-icons.min.js' %}"></script>
    </head>
    <body> 
    <!-- Info bar -->
<nav id="info_bar" style="background:#fec20b" class="uk-navbar-container" uk-navbar>
    <div class="uk-navbar-center">
        <ul class="uk-navbar-nav">
            <a href="tel:+961{{telephone}}" class="uk-navbar-item" uk-navbar="dropbar: true" style="color:#333"><span class="uk-margin-small-right" style="color:#333;" uk-icon="icon: phone; ratio: 1.5"></span></a>
            <a href="mailto:{{email}}?Subject=Milestone%20Inquiry" class="uk-navbar-item"><span style="color:#333; margin-right:5px;" uk-icon="icon: mail; ratio: 1.5"></span></a>
            <li>
                <a>{{current_lang}}</a>
                <div class="uk-navbar-dropbar" uk-dropdown="pos: bottom-justify">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li><a href="{{set_to_lang1}}">{{lang1}}</a></li>
                        <li><a href="{{set_to_lang2}}">{{lang2}}</a></li>
                    </ul>
                </div>
            </li>
        </ul>

    </div>

</nav>
<!-- END info bar-->

<!-- nav bar -->
    <nav id="normal_nav_bar" class="uk-navbar-container " uk-navbar {% if language == AR %} dir="rtl" {% endif %}>
        <div class="uk-navbar-center">
    
            <ul class="uk-navbar-nav">
                    
                    {% load static %}
                <a class="uk-navbar-item uk-logo" href="/"><img src="{% static 'homepage/images/logo.jpg' %}"></a>

                <li class="uk-active"><a  href="/">{{menu1}}</a></li>

                <li><a href="/programs">{{menu2}}</a></li>

                <li>
                    <a>{{menu3}}</a>
                    <div class="uk-navbar-dropdown" uk-dropdown="pos: bottom-justify">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li><a href="/classrooms">{{menu4}}</a></li>
                            <li><a href="/materials">{{menu5}}</a></li>
                            <li><a href="/teachers">{{menu6}}</a></li>
                            <li><a href="/faq">{{menu7}}</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a>{{menu8}}</a>
                    <div class="uk-navbar-dropdown" uk-dropdown="pos: bottom-justify">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li><a href="/visits">{{menu9}}</a></li>
                            <li><a href="/enroll-your-kid">{{menu10}}</a></li>
                            <li><a href="/fees">{{menu11}}</a></li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a>{{menu12}}</a>
                    <div class="uk-navbar-dropdown" uk-dropdown="pos: bottom-justify">
                        <ul class="uk-nav uk-navbar-dropdown-nav">                          
                            <li><a href="/watchyourkids">{{menu13}}</a></li>
                        </ul>
                    </div>
                </li>

                <li><a href="/aboutus">{{menu15}}</a></li>

                <li><a href="/connect" >{{menu16}}</a></li>                            
                
            </ul>
    
        </div>
                
    </nav>
<!-- END navbar -->

<!-- Mobile NavBar -->
    <nav id="mobile_nav_bar" class="uk-navbar-container" uk-navbar>
            <div class="uk-navbar-center">
        
                <ul class="uk-navbar-nav">
                        
                    <a class="uk-navbar-item uk-logo" href="#"><img src="{% static 'homepage/images/logo.jpg' %}"></a>

                    <button class="uk-navbar-item uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #offcanvas-nav-primary"><span class="uk-icon" uk-icon="icon: menu"></button>

                    </ul>
                </div>
        </nav>
                        
                    
    <div class="side_nav uk-offcanvas-content">
        <div id="offcanvas-nav-primary" uk-offcanvas="overlay: false">
            <div class="uk-offcanvas-bar uk-flex uk-flex-column mobile_nav_menu">
    
                <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
                        
                    <li class="uk-active"><a  href="#">{{menu1}}</a></li>

                    <li><a href="/programs" >{{menu2}}</a></li>

                    <li class="uk-parent">
                        <a>{{menu3}}</a>
                        <ul class="uk-nav-sub">
                                <li class="uk-nav-divider"></li>
                            <li><a href="/classrooms">{{menu4}}</a></li>
                            <li><a href="/materials">{{menu5}}</a></li>
                            <li><a href="/teachers">{{menu6}}</a></li>
                            <li><a href="/faq">{{menu7}}</a></li>
                        </ul>
                    </li>
                    
                    <li class="uk-parent">
                        <a href="#"  >{{menu8}}</a>           
                        <ul class="uk-nav-sub">
                                <li class="uk-nav-divider"></li>
                            <li><a href="/visits">{{menu9}}</a></li>
                            <li><a href="/enroll-your-kid">{{menu10}}</a></li>
                            <li><a href="/fees">{{menu11}}</a></li>
                        </ul>
                    </li>

                    <li class="uk-parent">
                        <a href="#"  >{{menu12}}</a>
                        <ul class="uk-nav-sub">
                                <li class="uk-nav-divider"></li>
                            <li><a href="/watchyourkids">{{menu13}}</a></li>
                        </ul>
                    </li>

                    <li><a href="/aboutus" >{{menu15}}</a></li>

                    <li><a href="/connect" >{{menu16}}</a></li>                            
                    
                </ul>
            </div>
        </div>
                                            
    </div>
<!-- END mobile Navbar -->

<div class="container">
    @yield('content')
</div>

<!-- Footer -->
<div   class="in_page_nav " style="width: 100%; display:block !important; background: #1c1f2a;">
    <div uk-grid class="uk-grid-large uk-child-width-expand@s uk-text-left">
        <!-- Left Side -->
        <div class="uk-width-2-3@m">
            <br><br>
            <p style="color:white;"> 
                {{rights}}
                <br>
                {{address}}
                <br>
                +961 {{telephone}}
            </p>
            <br>

            <span style="color:white;" uk-icon="icon: facebook"></span>
            <span style="color:white;" uk-icon="icon: twitter"></span>
            <span style="color:white;" uk-icon="icon: instagram"></span>

            <br><br>
        </div>
        <!-- Right Side -->
        <div  class="uk-width-1-3@m">

        </div>   
    </div> 
</div>
<!-- End Footer -->

</body>

</html>