<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocalizationController extends Controller
{
    public function index(Request $request, $locale){
        app()->setlocale($locale);
        return view('trying');
    }
}
